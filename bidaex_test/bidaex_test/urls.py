"""bdb0712 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path

from accounts.views import login,register,emailVerify,user_index,open_2FA,forgotpassword,index,resetPassword,logout,change_password
from trades.views import createorder,historyorder,cancelorder

urlpatterns = [
    re_path(r'^$',index ),
    re_path(r'^login/$', login, name='login'),
    re_path(r'^accounts/login/$', login, name='login'),
    re_path(r'^register/$', register),
    re_path(r'^emailVerify/$',emailVerify ),
    re_path(r'^user_index/$',user_index ),
    re_path(r'^setgoogle2fa/$',open_2FA ),
    re_path(r'^forgotpassword/$',forgotpassword ),
    re_path(r'^resetPassword/$',resetPassword),
    re_path(r'^logout/$', logout),
    re_path(r'^chpwd/$', change_password),

    re_path(r'^createorder/$', createorder ), #交易用的
    re_path(r'^historyorder/$', historyorder, name = 'historyorder'), #查詢三種歷史訂單
    re_path(r'^cancelorder/$', cancelorder, name = 'cancelorder'), #取消訂單

]