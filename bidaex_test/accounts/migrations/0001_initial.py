# Generated by Django 2.0.5 on 2018-08-13 02:48

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('phone', models.CharField(db_column='phone', max_length=20, null=True)),
                ('statement', models.IntegerField(db_column='statement', default=0, null=True)),
                ('ip', models.CharField(blank=True, db_column='ip', max_length=20)),
                ('language', models.CharField(db_column='language', max_length=20, null=True)),
                ('recommend_num', models.CharField(db_column='recommend_num', max_length=20, null=True)),
                ('recommend_num_from', models.CharField(db_column='recommend_num_from', max_length=20, null=True)),
                ('use_api', models.BooleanField(default=False)),
                ('use_2FA', models.BooleanField(default=False)),
                ('pass_2FA', models.BooleanField(default=False)),
                ('use_SMS', models.BooleanField(default=False)),
                ('use_Whitelist', models.BooleanField(default=False)),
                ('pass_SMS', models.BooleanField(default=False)),
                ('use_bdbfee', models.BooleanField(default=False)),
                ('login_sta', models.BooleanField(default=False)),
                ('mail_sta', models.BooleanField(default=False)),
                ('trade_sta', models.BooleanField(default=False)),
                ('withdraw_sta', models.BooleanField(default=False)),
                ('deposite_sta', models.BooleanField(default=False)),
                ('is_login', models.BooleanField(default=False)),
                ('cancel_time', models.IntegerField(db_column='cancel_time', default=0)),
                ('cancel_num', models.IntegerField(db_column='cancel_num', default=0)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(db_column='note', max_length=50)),
                ('address', models.CharField(db_column='address', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='api',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(db_column='note', max_length=20, null=True)),
                ('key', models.CharField(db_column='key', max_length=50, null=True)),
                ('secretkey', models.CharField(db_column='secretkey', max_length=50, null=True)),
                ('memuuid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='balance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total', models.FloatField(db_column='total', max_length=20)),
                ('canuse', models.FloatField(db_column='canuse', max_length=20)),
                ('freeze', models.FloatField(db_column='freeze', max_length=20)),
                ('btc', models.FloatField(db_column='btc', max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='coin',
            fields=[
                ('coin', models.CharField(db_column='coin', max_length=20, primary_key=True, serialize=False)),
                ('fullname', models.CharField(db_column='fullname', max_length=20)),
                ('minwithdrawal', models.FloatField(db_column='minwithdrawal', max_length=20)),
                ('exfee', models.FloatField(db_column='exfee', max_length=20)),
                ('imgurl', models.CharField(db_column='imgurl', max_length=50)),
                ('status', models.CharField(db_column='status', max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='exchangeinfo',
            fields=[
                ('symbol', models.CharField(db_column='symbol', max_length=20, primary_key=True, serialize=False)),
                ('status', models.CharField(db_column='status', max_length=20, null=True)),
                ('baseasset', models.CharField(db_column='baseasset', max_length=20, null=True)),
                ('baseassetprecision', models.IntegerField(db_column='baseassetprecision', null=True)),
                ('quoteasset', models.CharField(blank=True, db_column='quoteasset', max_length=20)),
                ('quoteprecision', models.IntegerField(db_column='quoteprecision', null=True)),
                ('ordertypes', models.CharField(db_column='ordertypes', max_length=20, null=True)),
                ('minprice', models.FloatField(db_column='minprice', max_length=20, null=True)),
                ('maxprice', models.FloatField(db_column='maxprice', max_length=20, null=True)),
                ('minQty', models.FloatField(db_column='minQty', max_length=20, null=True)),
                ('maxQty', models.FloatField(db_column='maxQty', max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='fee',
            fields=[
                ('fid', models.BigAutoField(db_column='fid', max_length=100, primary_key=True, serialize=False)),
                ('coin', models.CharField(db_column='coin', max_length=20)),
                ('name', models.CharField(db_column='name', max_length=20)),
                ('total', models.FloatField(db_column='total', default=0, max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='loginrecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.BigIntegerField(db_column='timestamp')),
                ('ip', models.CharField(db_column='ip', max_length=20)),
                ('city', models.CharField(db_column='city', max_length=20)),
                ('status', models.FloatField(db_column='status', max_length=50)),
                ('memuuid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='mailrecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.BigIntegerField(db_column='timestamp')),
                ('ip', models.CharField(db_column='ip', max_length=20)),
                ('url', models.CharField(db_column='url', max_length=50)),
                ('status', models.IntegerField(default=0)),
                ('memuuid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='realname',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('area', models.CharField(db_column='area', max_length=20, null=True)),
                ('lastname', models.CharField(db_column='lastname', max_length=20, null=True)),
                ('firstname', models.CharField(db_column='firstname', max_length=20)),
                ('idnumber', models.ImageField(db_column='idnumber', null=True, upload_to='')),
                ('idcardf', models.ImageField(db_column='idcardf', null=True, upload_to='')),
                ('idcardb', models.ImageField(db_column='idcardb', null=True, upload_to='')),
                ('idcardwithhand', models.ImageField(db_column='idcardwithhand', null=True, upload_to='')),
                ('memuuid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='userlevel',
            fields=[
                ('level', models.IntegerField(db_column='level', primary_key=True, serialize=False)),
                ('discount', models.FloatField(db_column='discount')),
                ('bdbdiscount', models.FloatField(db_column='bdbdiscount')),
            ],
        ),
        migrations.AddField(
            model_name='balance',
            name='coin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.coin'),
        ),
        migrations.AddField(
            model_name='balance',
            name='memuuid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='address',
            name='coin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.coin'),
        ),
        migrations.AddField(
            model_name='address',
            name='memuuid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='user',
            name='level',
            field=models.ForeignKey(default='0', on_delete=django.db.models.deletion.CASCADE, to='accounts.userlevel'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
    ]
